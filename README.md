# Incubator Submission

Welcome to the GNOME Core and Development app incubator submission.

A submission for incubation is the first step for an app to make its way into GNOME Core or GNOME Development. The complete process is described in [App Lifecycle](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppLifecycle.md) document - please read this, including the section on the incubation process, before submitting your app.

If you have any questions feel free to contact the Design or Release Team.

  - **[Submit your app](https://gitlab.gnome.org/Incubator/Submission/-/issues/new)**
